$(document).ready(function () {
	$(".main-slider-wrap").slick({
		slidesToShow: 1,
        arrows: true,
		slidesToScroll: 1,
        prevArrow: '<button class="prev1 slide-arrows"></button>',
        nextArrow: '<button class="next1 slide-arrows"></button>',
		dots: true,
		fade: true,
        speed: 900,
        infinite: true,
        cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        responsive: [
            {
                breakpoint: 576,
                settings:{
                    arrows: false
                }
            }
        ]
	});

	$(".spec-offers-slider-wrap").slick({
        slidesToShow: 3,
        arrows: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: '<button class="prev2 slide-arrows2"></button>',
        nextArrow: '<button class="next2 slide-arrows2"></button>',
        responsive: [
            {breakpoint: 991,
                settings:{
                    slidesToShow: 2
                }
            },
            {breakpoint: 768,
                settings:{
                    slidesToShow: 1
                }
            },
            {breakpoint: 576,
                settings:{
                    slidesToShow: 1
                }
            }
        ]
    });

	// toggle menu

    var trigger = $('.hamburglar');
    var isClosed = false;

    $(document).on('click', '.navbar-toggler', function() {
        if (isClosed == true) {
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    });

    $(".paym-sys-wrap").slick({
        arrows: false,
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {breakpoint:786,
                settings: {
                slidesToShow: 4
                }
            },
            {breakpoint:480,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

    $(".gallery-album-full-slider").slick({
        slidesToShow: 1,
        fade: true,
        arrows: false,
        asNavFor: '.gallery-album-min-slider'
    });

    $(".gallery-album-min-slider").slick({
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        focusOnSelect: true,
        arrows: true,
        asNavFor: '.gallery-album-full-slider',
        prevArrow: '<button class="prev3 full-slide-arrows"></button>',
        nextArrow: '<button class="next3 full-slide-arrows"></button>',
        responsive: [
            {
                breakpoint:992,
                settings:{
                  slideToShow: 3,
                    vertical: false,
                    verticalSwiping: false,
                    arrows: false
                }
            }
        ]
    });

    $(".fancybox-opened").fancybox({
        transitionEffect: 'zoom-in-out'
    });

    $(".faq-collapse-btn").on("click", function () {
        $(this).closest(".faq-ques-box").find(".faq-ques-txt").slideToggle();
       $(this).toggleClass("collapsed");
    });

    $(".our-client-slider").slick({
        slidesToShow: 5,
        prevArrow: '<button class="prev2 slide-arrows2"></button>',
        nextArrow: '<button class="next2 slide-arrows2"></button>',
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint:768,
                settings: {
                    slidesToShow:4
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow:3,
                    arrows: false
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            }
        ]
    })

});